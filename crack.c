#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=34;        // Length of MD5 hash strings


struct entry 
{
    char * word;
    char * hash;
};

int sortHashes(const void *a, const void *b) {
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    return strcmp(aa->hash, bb->hash);
}

int hashCompare(const void *key, const void *elem) {
    char *kk = (char *)key;
    struct entry *ee = (struct entry *)elem;
    return strcmp(kk, ee->hash);
}

struct entry *read_dictionary(char *filename, int *size)
{
    struct stat info;
    if(stat(filename, &info) == -1) {
        printf("Can't stat the file\n");
        exit(1);
    }
    int fileSize = info.st_size;
    
    printf("File is %d bytes\n", fileSize);
    
    char *content = malloc(fileSize + 1);
    FILE *inputFile = fopen(filename, "r");
    if (inputFile == NULL) {
        printf("Can't open file %s", filename);
        exit(1);
    }
    fread(content, 1, fileSize, inputFile);
    fclose(inputFile);
    content[fileSize] = '\0';
    
    int lineCount = 0;
    for (int i=0; i < fileSize; i++) {
        if (content[i] == '\n') {
            lineCount++;
        }
    }
    
    struct entry *entries = malloc(lineCount * sizeof(struct entry));
    char **words = malloc(lineCount * sizeof(char *));
   
    words[0] = strtok(content, "\n");
    entries[0].word = words[0];
    entries[0].hash = md5(words[0], strlen(words[0]));
    int i=1;
    while ((words[i] = strtok(NULL, "\n")) != NULL) {
        entries[i].word = words[i];
        entries[i].hash = md5(words[i], strlen(words[i]));
        i++;
    }
    
    free(words);
    *size = lineCount;
    return entries;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    int dlen;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    qsort(dict, dlen, sizeof(struct entry), sortHashes);

    FILE *hashFile = fopen(argv[1], "r");
    if (hashFile == NULL) {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }

    char hash[HASH_LEN];
    while (fgets(hash, HASH_LEN, hashFile) != NULL) {
        *strchr(hash, '\n') = '\0';
        
        struct entry *result = bsearch(hash, dict, dlen, sizeof(struct entry), hashCompare);
        if (result) {
            printf("%s %s\n", result->hash, result->word);
        } else {
            printf("Hash Not Found.\n");
        }
    }
    
    fclose(hashFile);
    free(dict);
}
